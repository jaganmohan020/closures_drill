function limitFunctionCallCount(cb,n){
    let count = 0;

    function invoke(...x){
        count++

        if(isNaN(n) == true || cb == undefined || typeof cb !== "function"){
            throw new Error("This is not defined")

        } else {
            
            if (count <= n){
                    return cb(...x);
                }
            else{
                    return "Callback already called " + n + "-times";
                }
        }

      
    }
    return invoke;
}



module.exports = limitFunctionCallCount;