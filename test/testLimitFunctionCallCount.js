const limitFunctionCallCount = require("../limitFunctionCallCount");


const sum  = (a,b) => a + b;
const diff = (a,b) => a - b;
const sumThree = (a,b,c) => a + b + c;
const helloCallback = (x) => x;

const sum2 = limitFunctionCallCount(sum,1);
const diff2 = limitFunctionCallCount(diff,1);
const sum3 = limitFunctionCallCount(sumThree,1);
const hello = limitFunctionCallCount(helloCallback,1);

try{
    console.log(sum2(4,3))
    console.log(diff2(5,4))
    console.log(sum3(6,7,8))
    console.log(hello("hello"))
    
    console.log(sum2(4,3))
    console.log(diff2(5,4))
    console.log(sum3(6,7,8))
    console.log(hello("hello"))
    
} catch (error) {
    console.log("There are errors in code.")
}