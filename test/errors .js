/* • Your limitFunctionCallCount implementation did not throw an error when there were no or partial parameters passed to it.
     Instead of returning a random data type, you can throw an error when the parameters fail a data check.
     You will then have to use try catch inside your test file to verify that the function is throwing the correct error message.: expected [Function] to throw an error



// • Your limitFunctionCallCount implementation did not invoke the callback correctly when the returned function was called. 
     It should invoke it with all the parameters that were passed to the returned function.
     There is no fixed number of parameters that the callback function will take.: expected spy to have been called with exact arguments 


// • Your limitFunctionCallCount implementation did not return null when the returned function was called more than the provided limit.
     Make sure that you read and understand the problem before starting to write the code: expected undefined to equal null


// • Your limitFunctionCallCount implementation did not return the correct value when the returned function was called.
     Make sure that you read and understand the problem before starting to write the code: expected undefined to equal 4


// • Your limitFunctionCallCount implementation did not invoke the callback correctly when the returned function was called.
     It should invoke it with all the parameters that were passed to the returned function. 
     There is no fixed number of parameters that the callback function will take.: expected spy to have been called with exact arguments 2, 3



// • Your limitFunctionCallCount implementation did not invoke the callback correctly when the returned function was called. 
    It should invoke it with all the parameters that were passed to the returned function. 
    There is no fixed number of parameters that the callback function will take.: expected spy to have been called with exact arguments 2, 3, 4  */