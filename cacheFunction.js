function cacheFunction(cb) {
  const cache = new Set();

  function invoke ( param ) {
    if (!cache.has(param)) {
      console.log(cache);
      cache.add(param);
      return cb(param);
    } 
    else {
      console.log(cache);
      return cache;
    }
  } 
  return {invoke};
}


module.exports = cacheFunction;

